﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Mahric;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestCentimes()
        {
            string strnull = null;
            Assert.AreEqual("et zero centimes", Centimes.Centime(strnull));
            string dectrad = "quarante-deux";
            string strtest = Centimes.Centime(dectrad);
            Assert.AreEqual("et quarante-deux centimes", strtest);
        }

        [TestMethod]
        public void TestCentaines()
        {
            string strtest = "";
            int[] tab = new int[3] { 4, 5, 3 };
            strtest = Centaines.Centaine(tab);
            Assert.AreEqual("quatre cent", strtest);
            int[] tab2 = new int[3] { 4, 0, 0 };
            strtest = Centaines.Centaine(tab2);
            Assert.AreEqual("quatre cents", strtest);
            int[] tab3 = new int[3] { 1, 0, 0 };
            strtest = Centaines.Centaine(tab3);
            Assert.AreEqual("cent", strtest);
            int[] tab4 = new int[3] { 0, 9, 9 };
            strtest = Centaines.Centaine(tab4);
            Assert.AreEqual("", strtest);
        }
    }
}
