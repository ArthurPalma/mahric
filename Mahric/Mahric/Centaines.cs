﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mahric
{
    // Fonction traduisant la partie numérique des centaines en toutes lettres
    public class Centaines
    {
        public static string Centaine(int[] tab)
        {
            string str_unit = "";
            if (tab[0] == 2)
            {
                str_unit = "deux";
            }
            if (tab[0] == 3)
            {
                str_unit = "trois";
            }
            if (tab[0] == 4)
            {
                str_unit = "quatre";
            }
            if (tab[0] == 5)
            {
                str_unit = "çinq";
            }
            if (tab[0] == 6)
            {
                str_unit = "six";
            }
            if (tab[0] == 7)
            {
                str_unit = "sept";
            }
            if (tab[0] == 8)
            {
                str_unit = "huit";
            }
            if (tab[0] == 9)
            {
                str_unit = "neuf";
            }
            string str = "";
            if (tab[0] != 0)
            {
                if (tab[0] == 1)
                {
                    str ="cent";
                }
                if ((tab[0] > 1) && (tab[1] == 0) && (tab[2] == 0))
                {
                    str = str_unit + " cents";
                }
                if ((tab[0] > 1) && (tab[1] != 0) && (tab[2] == 0))
                {
                    str = str_unit + " cent";
                }
                if ((tab[0] > 1) && (tab[1] != 0) && (tab[2] == 0))
                {
                    str = str_unit + " cent";
                }
                if ((tab[0] > 1) && (tab[1] != 0) && (tab[2] != 0))
                {
                    str = str_unit + " cent";
                }
            }
            return str;
        }
    }
}
