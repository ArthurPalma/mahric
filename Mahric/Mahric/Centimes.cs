﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mahric
{
    // Fonction permettant de distinguer la partie centime du reste
    public class Centimes
    {
        public static string Centime(string centimes)
        {
            string str = "et zero centimes";
            if (centimes != null)
            {
                str = "et " + centimes + " centimes";
            }
            return str;
        }
    }
}
